"""Includes unit tests for virtual_env_init."""
import os

from init_repo import virtual_env_init


def test_main(tmpdir):
    """Tests the main function."""
    with tmpdir.as_cwd():
        assert not os.path.exists('venv')
        assert not os.path.exists('.gitignore')
        assert virtual_env_init.get_version() is None

        virtual_env_init.main()

        assert os.path.exists('venv')
        assert os.path.exists('.gitignore')
        assert virtual_env_init.get_version() == 1
