"""Module that tests the hooks_init setup script."""
import os

from init_repo import hooks_init


def test_main(venv_directory):
    """Tests the hooks_init setup script."""
    with venv_directory.as_cwd():
        assert hooks_init.get_version() is None
        assert not os.path.isdir('hooks')

        hooks_init.main()

        assert hooks_init.get_version() is not None
        assert os.path.isdir('hooks')
        assert os.path.exists(os.path.join('hooks', 'pre-commit.py'))
        assert os.path.isdir(os.path.join('.git', 'hooks'))
        assert os.path.isfile(os.path.join('.git', 'hooks', 'pre-commit'))
