"""Test file for the git_init setup script."""

import os

from init_repo import git_init


def test_get_version(tmpdir):
    """Tests the get_version and init_repo functions."""
    with tmpdir.as_cwd():
        assert git_init.get_version() is None

        assert not os.path.exists('.\\.git')

        git_init.init_repo()

        assert os.path.exists('.\\.git')

        assert git_init.get_version() == 1


def test_create_gitignore(tmpdir):
    """Tests the create_gitignore function."""
    with tmpdir.as_cwd():
        assert not os.path.exists('.gitignore')

        git_init.create_gitignore()

        assert os.path.exists('.gitignore')


def test_main(tmpdir):
    """Tests the main function."""
    with tmpdir.as_cwd():
        assert not os.path.exists('.\\.git')
        assert not os.path.exists('.gitignore')

        git_init.main()

        assert os.path.exists('.\\.git')
        assert os.path.exists('.gitignore')
