import os

import pytest

from init_repo import testing_init
from init_repo import utils


def version_test(version):
    """Tests the version of the project for testing_init.

    An assert is performed comparing the project version to the argument
    version.

    Args:
        version (int): None if there is no project version or an integer
            representing the project version.
    """
    result = testing_init.get_version()
    if version is None:
        assert result is None
    else:
        assert result == version


def test_setup_tests(tmpdir):
    """Tests the setup tests function."""
    with tmpdir.as_cwd():
        test_path = os.path.join(os.getcwd(), 'tests')
        assert not os.path.isdir(test_path)
        version_test(0)

        testing_init.setup_tests()
        assert os.path.isdir(test_path)
        assert os.path.isfile(os.path.join(test_path, '__init__.py'))
        version_test(1)


def test_add_pytest_to_config(tmpdir):
    """Tests the add_pytest_to_config function."""
    with tmpdir.as_cwd():
        version_test(0)

        assert not os.path.isfile('setup.cfg')
        testing_init.add_pytest_to_config()
        assert os.path.isfile('setup.cfg')

        with utils.config_read() as config:
            assert 'pytest' in config

        version_test(0)

        tests_path = os.path.join(os.getcwd(), 'tests')
        utils.create_dir(tests_path)

        version_test(2)


def test_add_coverage_to_config(tmpdir):
    """Tests the add_coverage_to_config function."""
    with tmpdir.as_cwd():
        assert not os.path.isfile('setup.cfg')
        testing_init.add_coverage_to_config()
        assert os.path.isfile('setup.cfg')

        with utils.config_read() as config:
            assert 'coverage:run' in config
            assert 'coverage:report' in config


def test_main(venv_directory):
    with venv_directory.as_cwd():
        version_test(0)
        assert not os.path.isdir('tests')

        testing_init.main()

        version_test(2)
        assert os.path.isdir('tests')

