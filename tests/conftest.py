"""Test configuration"""
import os

import pytest

from init_repo import virtual_env_init


VENV_TEMPLATE = os.path.join('tests', 'resources', 'venv_template.zip')


@pytest.fixture()
def venv_directory(tmpdir):
    """Pytest fixture that returns a directory with a virtual environment set
    up.

    Arguments:
        tmpdir (py.path.local): This fixture utilizes the default pytest
            tmpdir fixture.

    Returns:
        py.path.local: The path to the temporary repository folder
    """
    with tmpdir.as_cwd():
        virtual_env_init.main()
    return tmpdir
