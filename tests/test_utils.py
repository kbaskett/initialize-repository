"""Module that tests the functions in utils.py that aren't covered by other
tests."""
import os

import pytest

from init_repo import utils


def test_create_dir(tmpdir):
    """Tests the create_dir function."""
    folder_path = os.path.join(str(tmpdir), 'test_dir')
    assert not os.path.isdir(folder_path)
    utils.create_dir(folder_path)
    assert os.path.isdir(folder_path)
    # Ensure a second call does not raise an exception.
    utils.create_dir(folder_path)
    assert os.path.isdir(folder_path)


def test_virtualenv_active_failure(tmpdir):
    """Tests virtualenv_active when no virtual environment exists."""
    with tmpdir.as_cwd():
        with pytest.raises(NotADirectoryError):
            with utils.virtualenv_active():
                pass
