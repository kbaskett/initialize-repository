"""Module that tests the docs_init setup script."""
import os

from init_repo import docs_init
from init_repo import utils


def test_main(venv_directory):
    """Tests the docs_init setup script."""
    with venv_directory.as_cwd():
        assert docs_init.get_version() is None
        assert not os.path.isdir('docs')

        docs_init.main()

        assert docs_init.get_version() == 1
        assert os.path.isdir('docs')
        assert os.path.exists(os.path.join('docs', 'Makefile'))


def test_install_sphinx(venv_directory, capsys):
    with venv_directory.as_cwd():
        with utils.virtualenv_active():
            _, _ = capsys.readouterr()
            docs_init.install_sphinx()
            out, _ = capsys.readouterr()
            assert out.startswith('\nInstalling sphinx...')
            assert 'Collecting sphinx' in out
            assert 'Successfully installed' in out
