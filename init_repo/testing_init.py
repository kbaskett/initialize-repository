"""Script for setting up a testing environment in the current repository.

1. Creates a tests directory.
2. Adds empty __init__.py inside the tests directory.
3. Installs pytest and coverage in the repository's virtual environment.
"""
import os
from .utils import (
    get_virtual_envs,
    virtualenv_active,
    create_dir,
    git_call,
    git_ignore,
    pip_install,
    config_read,
    config_update,
    staged_files
)


CURRENT_VERSION = 2
SORT_ORDER = 50


def main():
    """Main method for the script."""
    repo_version = get_version()
    if repo_version < 1:
        setup_tests()
        with virtualenv_active():
            install_pytest()
            install_coverage()

    if repo_version < 2:
        add_pytest_to_config()
        add_coverage_to_config()

    if staged_files():
        git_call('commit', '--message=Committing testing files')


def get_version():
    """Determine the version of this script that was most recently run on the
    repository.

    Returns:
        int: The version of the script that was most recently run on the
            repository.
    """
    tests_dir = os.path.join(os.getcwd(), 'tests')
    if not os.path.isdir(tests_dir):
        return 0
    if not os.path.isfile('setup.cfg'):
        return 1

    with config_read() as config:
        if 'pytest' not in config:
            return 1
        elif 'norecursedirs' not in config['pytest']:
            return 1
        elif 'init_repo' not in config['pytest']['norecursedirs']:
            return 1

    return 2


def setup_tests():
    """Sets up a tests module in the current project directory."""
    tests_path = os.path.join(os.getcwd(), 'tests')
    print('\nSetting up tests module in {0}...'.format(tests_path))
    create_dir(tests_path)
    init_path = os.path.join(tests_path, '__init__.py')
    open(init_path, 'a').close()

    git_call('add', init_path)

    git_ignore(['.cache/', '.coverage', 'htmlcov/', '__pycache__'])


def add_pytest_to_config():
    """Updates setup.cfg with pytest exclusions.

    Pytest is setup to not look for tests in the following directories:
    - .git
    - .svn
    - docs
    - init_repo
    - Any virtual environments
    """
    print('\nUpdating setup.cfg with pytest exclusions...')
    with config_update() as config:
        config['pytest'] = {}
        config['pytest']['norecursedirs'] = '.git .svn docs init_repo'
        for venv_path in get_virtual_envs():
            config['pytest']['norecursedirs'] += (' ' +
                                                  os.path.basename(venv_path))

    git_call('add', 'setup.cfg')


def add_coverage_to_config():
    """Updates setup.cfg with coverage exclusions.

    Coverage is set up to not display results for the following subdirectories:
    - init_repo
    - tests
    - any virtual environments
    """
    print('\nUpdating setup.cfg with coverage exclusions...')
    with config_update() as config:
        config['coverage:run'] = {}
        run = config['coverage:run']
        run['omit'] = '\n\tinit_repo\\* \n\ttests\\*'

        config['coverage:report'] = {}
        report = config['coverage:report']
        report['omit'] = '\n\tinit_repo\\* \n\ttests\\*'

        for venv_path in get_virtual_envs():
            subpath = ' \n\t' + os.path.basename(venv_path) + os.sep + '*'
            run['omit'] += subpath
            report['omit'] += subpath

    git_call('add', 'setup.cfg')


def install_pytest():
    """Installs pytest."""
    print('\nInstalling pytest...')
    pip_install('pytest')


def install_coverage():
    """Installs coverage."""
    print('\nInstalling coverage...')
    pip_install('coverage')


if __name__ == '__main__':
    main()  # pragma: no cover
