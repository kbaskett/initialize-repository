"""Script for setting a the current repository for documentation generation.

1. Installs sphinx in the repository's virtual environment.
2. Creates a docs directory in the repository.
3. Runs sphinx-quickstart in the directory.
4. Commits the docs directory.
"""
import os
import subprocess
from .utils import (
    virtualenv_active,
    create_dir,
    git_call,
    pip_install,
    git_ignore
)


CURRENT_VERSION = 1
SORT_ORDER = 50


def main():
    """Main method for the script."""
    repo_version = get_version()
    if repo_version is None:
        setup_docs()
        with virtualenv_active():
            install_sphinx()
            sphinx_quickstart()
        commit_docs()


def get_version():
    """Determine the version of this script that was most recently run on the
    repository.

    Returns:
        int: The version of the script that was most recently run on the
            repository. If the script has not yet been run, return None.
    """
    docs_dir = os.path.join(os.getcwd(), 'docs')
    if os.path.isdir(docs_dir):
        return 1
    else:
        return None


def setup_docs():
    """Sets up a docs directory in the current project directory."""
    docs_path = os.path.join(os.getcwd(), 'docs')
    print('\nSetting up docs directory in {0}'.format(docs_path))
    create_dir(docs_path)
    git_ignore(['docs/build/'])


def install_sphinx():
    """Installs sphinx."""
    print('\nInstalling sphinx...')
    pip_install('sphinx')


def sphinx_quickstart():
    """Runs sphinx-quickstart in the docs directory."""
    print('\nRunning sphinx-quickstart...')
    content_dir = os.path.join(os.getcwd(), 'docs')
    project_name = os.path.basename(os.getcwd())
    user_name = git_call('config', 'user.name')

    # Need to add an extra keystroke to the end of input_stream so the
    # process doesn't exit prematurely.
    input_stream = ('{0}\ny\n\n{1}\n{2}\n0.1\n0.1\nen\n\n\n\ny\n\n\ny\ny\n\n'
                    '\n\n\ny\ny\n\n\n').format(content_dir, project_name,
                                               user_name)
    proc = subprocess.Popen(('sphinx-quickstart', ), stdin=subprocess.PIPE)
    proc.communicate(input=input_stream.encode())


def commit_docs():
    """Commits the new docs directory and configuration files."""
    print('\nCommitting docs directory...')
    git_call('add', './docs/')
    git_call('commit', '--message=Base documentation directory')


if __name__ == '__main__':
    main()  # pragma: no cover
