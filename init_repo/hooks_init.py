import glob
import os
import shutil
import tempfile
import urllib.request
import zipfile

from .utils import (
    create_dir,
    config_read,
    config_update,
    change_dir,
    git_call,
    get_virtual_envs,
    staged_files,
    pip_install,
    virtualenv_active
)

CURRENT_VERSION = 1
SORT_ORDER = 80

DOWNLOAD_URL = 'https://bitbucket.org/kbaskett/githooks/get/master.zip'

HOOK_TEXT = """#!/bin/sh
{python_path} hooks/{hook_name}
"""


def main():
    version = get_version()
    previous_hooks = list_existing_hooks()
    with tempfile.TemporaryDirectory() as work_dir:
        with change_dir(work_dir):
            download_hooks()
            with zipfile.ZipFile('hooks_master.zip') as zip_file:
                hooks_version = get_hooks_version(zip_file)

                if hooks_version != version:
                    extract_hooks(zip_file)

        if hooks_version != version:
            copy_hooks(work_dir)
            write_version(hooks_version)
            git_call('add', 'setup.cfg')

            # Only commit hooks if there was no hooks folder there to start
            # with. Otherwise, changes might have been made to the hooks, so
            # they may need to retain some of those changes
            if version is None:
                git_call('add', 'hooks')

            if staged_files():
                print('\nCommitting hooks directory...')
                git_call('commit',
                         '--message=Pre-commit and post-commit hooks')

    git_hooks_path = os.path.join('.git', 'hooks')
    if not os.path.exists(git_hooks_path):
        print('\nCreating .git/hooks...')
        create_dir(git_hooks_path)

    current_hooks = list_existing_hooks()
    update_git_hooks(previous_hooks, current_hooks)

    if version is None:
        install_dependencies()


def get_version():
    """Returns a version number representing the version of hooks installed.

    If the hooks folder does not exist, None is returned.
    If there is a githooks/version key in setup.cfg, then that version is
    returned.
    Otherwise, 1 is returned.

    Returns:
        str: A string representing the version of GitHooks installed or None
    """
    version = None
    if os.path.exists('hooks'):
        with config_read() as config:
            if 'githooks' not in config or 'version' not in config['githooks']:
                version = "1"
            else:
                version = config['githooks']['version']

    print('\nExisting GitHooks version: %s' % version)
    return version


def list_existing_hooks():
    """Lists the hooks that exist in the hooks folder.

    Returns:
        set: A list of hook file names
    """
    hooks = set()
    for hook_path in glob.iglob('hooks\\*.py'):
        hook_name = os.path.basename(hook_path)
        if hook_name in ('__init__.py', 'utils.py'):
            continue
        hooks.add(hook_name)
    return hooks


def download_hooks():
    """Downloads the most recent version of the GitHooks module.

    The master branch of GitHooks is downloaded from bitbucket.
    """
    print('\nDownloading GitHooks package...')
    response = urllib.request.urlopen(DOWNLOAD_URL)
    with open('hooks_master.zip', 'wb') as download:
        download.write(response.read())


def get_hooks_version(zip_file):
    """Gets the version of the GitHooks package in the specified zip folder.

    In the zip folder, the top level folder includes the changeset number,
    and that is used as the version number.

    Arguments:
        zip_file (zipfile.ZipFile): An object representing the zip file.

    Returns:
        str: The version of GitHooks included in the zip file
    """
    info = zip_file.infolist()[0]
    top_folder = info.filename.split('/')[0]
    version = top_folder.split('-')[-1]
    print('\nDownloaded GitHooks version: %s' % version)
    return version


def extract_hooks(zip_file):
    """Extracts the hooks directory from the zip file.

    Arguments:
        zip_file (zipfile.ZipFile): An object representing the zip file
    """
    print('\nExtracting hooks...')
    for info in zip_file.infolist():
        partial_path = info.filename
        folders = partial_path.split('/')
        if folders[1] == 'hooks':
            path_ = os.path.join(*folders[1:])
            if not folders[-1]:
                print('Creating folder %s' % path_)
                create_dir(path_)
                continue
            print('Writing file %s' % path_)
            with open(path_, 'wb') as file_:
                file_.write(zip_file.read(info))


def copy_hooks(work_dir):
    """Copies the hooks folder from the temporary working directory to the
    main hooks directory.

    Arguments:
        work_dir (str): Path to the working directory where the hooks folder
            was extracted
    """
    if os.path.exists('hooks'):
        print('\nDeleting hooks...')
        shutil.rmtree('hooks')

    print('\nInstalling hooks...')
    shutil.copytree(os.path.join(work_dir, 'hooks'), 'hooks')


def write_version(version):
    """Writes the GitHooks version to setup.cfg.

    Args:
        version (str): The version string to write
    """
    with config_update() as config:
        if 'githooks' not in config:
            config['githooks'] = {}
        config['githooks']['version'] = version


def update_git_hooks(previous_hooks, current_hooks):
    """Updates the set of git hooks pointer to the scripts in the hooks folder.

    The current and previous lists of scripts are compared. If any scripts
    no longer exist, those hooks are removed. New hooks are added for any


    Arguments:
        previous_hooks (set): Set of script names that existed at the beginning
            of this setup script
        current_hooks (set): Set of script names that existed after the new
            version of GitHooks was installed
    """
    git_hooks_path = os.path.join('.git', 'hooks')
    # delete hooks that no longer exist
    for hook_name in previous_hooks - current_hooks:
        hook_path = os.path.join(git_hooks_path,
                                 os.path.splitext(hook_name)[0])
        print('Deleting', hook_path)
        os.remove(hook_path)

    try:
        virtual_env = os.path.basename(get_virtual_envs()[0])
    except IndexError:
        virtual_env = None
    # Create hooks that are new
    for hook_name in current_hooks - previous_hooks:
        hook_path = os.path.join(git_hooks_path,
                                 os.path.splitext(hook_name)[0])
        print('Writing', hook_path)
        with open(hook_path, 'w') as file_:
            file_.write(get_hook_text(hook_name, virtual_env))


def get_hook_text(hook_name, virtual_env=None):
    """Returns text for the git hook file.

    Whenever a python script is added to the top-level hooks folder, a shell
    script must be added to .git/hooks so git will run the script. If there
    is a virtual environment defined in this folder, that will be used to run
    the python script.

    Arguments:
        hook_name (str): Name of the python script to execute. For example,
            post-commit.py.
        virtual_env (str): Folder name for the virtual environment. For
            example, venv. (default: {None})

    Returns:
        str: Text for the shell script file
    """
    python_path = 'python'
    if virtual_env:
        python_path = './{0}/Scripts/python'.format(virtual_env)

    return HOOK_TEXT.format(python_path=python_path, hook_name=hook_name)


def install_dependencies():
    """Installs dependencies needed by GitHooks.

    - pep8
    - pep8ify
    """
    print('\nInstalling dependencies...')
    with virtualenv_active():
        print('Installing pep8')
        pip_install('pep8')
        print('Installing pep8ify')
        pip_install('pep8ify')


if __name__ == '__main__':
    main()  # pragma: no cover
