"""Contains utilities for the Repository Initialization scripts."""
import configparser
import contextlib
import errno
import functools
import glob
import os
import re
import subprocess


MODIFIED_RE = re.compile(r"^[AM]+\s+(?P<name>.*)", re.MULTILINE)


def git_call(subcmd, args=None):
    """Calls the given git command.

    Args:
        subcmd (str): Git subcommand
        args (str or list): Arguments to the subcommand (default: {None})

    Returns:
        str: The output of the command

    Raises:
        subprocess.CalledSubprocessError: If the command fails for any reason,
            a CalledSubprocessError is raised.
    """
    if subcmd != 'init' and not has_git():
        return ''

    cmd = ['git', subcmd]
    if args:
        if isinstance(args, str):
            cmd.append(args)
        else:
            for arg in args:
                cmd.append(arg)
    return subprocess.check_output(cmd).decode()


def has_git():
    """Determine whether git is setup for the current working directory.

    Currently, this just checks for the existence of a .git directory.

    Returns:
        bool: True if git is configured for the repository. False otherwise.
    """
    return os.path.isdir(os.path.join(os.getcwd(), '.git'))


def git_ignore(patterns):
    """Add the specified patterns to the .gitignore file.

    The lines are appended to the end of the .gitignore file in the current
    working directory.

    Arguments:
        patterns (list): List of strings representing patterns to ignore.
    """
    path = os.path.join(os.getcwd(), '.gitignore')
    with open(path, 'a') as gitignore:
        for pat in patterns:
            gitignore.write(pat + '\n')


@contextlib.contextmanager
def virtualenv_active():
    """Context manager that activates a virtual environment for subprocess calls.

    The context manager works by monkey patching subprocess.Popen. The
    existing Popen command is replaced by a function that has the shell and
    env keyword arguments specified. The shell argument is set to True. The
    env argument is set to the current value of os.environ with the path to
    the virtual environment Scripts directory prepended.

    Raises:
        NotADirectoryError: An error is raised if no virtual environment is
            found in the current working directory
    """
    venvs = get_virtual_envs()
    if not venvs:
        raise NotADirectoryError(
            'No virtual environment exists in the current working directory:' +
            os.getcwd())

    # Set up the new environment
    local_env = os.environ.copy()
    for venv in venvs:
        scripts = os.path.join(venv, 'Scripts')
        local_env['PATH'] = scripts + ';' + local_env['PATH']

    print('Activating virtual environment')
    # Monkeypatch subprocess.Popen
    existing_popen = subprocess.Popen
    subprocess.Popen = functools.partial(existing_popen, shell=True,
                                         env=local_env)
    try:
        yield
    finally:
        # Restore subprocess.Popen to the original version
        subprocess.Popen = existing_popen
        print('Deactivating virtual environment')


def get_virtual_envs():
    """Return a list of the virtual environment paths in the current working
    directory.

    This function uses a glob search to find all the Scripts/python.exe files
    in the current working directory.

    Returns:
        list: A list of paths to virtual environment directories in the
            current working directory
    """
    venv_glob = os.path.join(os.getcwd(), '*', 'Scripts', 'python.exe')
    return [os.path.dirname(os.path.dirname(path)) for path in
            glob.iglob(venv_glob)]


def create_dir(path):
    """Create a directory if it doesn't exist.

    Arguments:
        path (str): The path to a directory
    """
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def pip_install(package):
    """Uses pip to install the specified package.

    Args:
        package (str): The name of a package to install
    """
    proc = subprocess.Popen(('pip', 'install', '--disable-pip-version-check', package),
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output, _ = proc.communicate()
    print(output.decode())


@contextlib.contextmanager
def config_read():
    """Context manager for reading from the project config file.

    Yields:
        configparser.ConfigParser: A ConfigParser object is returned to handle
            reading from the config file.
    """
    config = configparser.ConfigParser()
    config.read('setup.cfg')
    try:
        yield config
    finally:
        pass


@contextlib.contextmanager
def config_update():
    """Context manager for updating the project config file.

    Yields:
        configparser.ConfigParser: A ConfigParser object is returned to handle
            reading from the config file.
    """
    config = configparser.ConfigParser()
    config.read('setup.cfg')
    try:
        yield config
    finally:
        with open('setup.cfg', 'w') as configfile:
            config.write(configfile)


def staged_files():
    """Get all files staged for the current commit."""
    return MODIFIED_RE.findall(git_call('status', '--porcelain'))


@contextlib.contextmanager
def change_dir(working_dir):
    """Context manager to change the current working directory.

    Arguments:
        working_dir (str): Path to the new working directory.
    """
    prev_working_dir = os.getcwd()
    os.chdir(working_dir)
    try:
        yield
    finally:
        os.chdir(prev_working_dir)
