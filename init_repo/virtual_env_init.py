"""Script for setting up a virtualenv for the project

1. Sets up the virtualenv.
2. Adds the path to the .gitignore file.
"""
import os
import venv
from .utils import git_ignore


CURRENT_VERSION = 1
SORT_ORDER = 20


def main():
    """Main method for the script."""
    repo_version = get_version()
    if repo_version is None:
        setup_venv()
        ignore_venv()


def get_version():
    """Determine the version of this script that was most recently run on the
    repository.

    Returns:
        int: The version of the script that was most recently run on the
            repository. If the script has not yet been run, return None.
    """
    venv_dir = os.path.join(os.getcwd(), 'venv')
    if os.path.isdir(venv_dir):
        return 1
    else:
        return None


def setup_venv():
    """Sets up a virtualenv in the current project directory."""
    print('Setting up virtualenv in {0}'.format(
        os.path.join(os.getcwd(), 'venv')))
    venv.create('venv', with_pip=True)


def ignore_venv():
    """Adds the venv directory to the .gitignore list."""
    git_ignore(['venv/'])


if __name__ == '__main__':
    main()  # pragma: no cover
