"""Script for setting up a git repo for this repository.

1. Initializes the git repository.
2. Sets up a gitignore file to ignore the initialization files.
"""
import os
from .utils import git_call, git_ignore


CURRENT_VERSION = 1
SORT_ORDER = 10


def main():
    """Main method for the script."""
    repo_version = get_version()
    if repo_version is None:
        init_repo()
        create_gitignore()


def get_version():
    """Determine the version of this script that was most recently run on the
    repository.

    [description]

    Returns:
        int: The version of the script that was most recently run on the
            repository. If the script has not yet been run, return None.
    """
    git_dir = os.path.join(os.getcwd(), '.git')
    if os.path.isdir(git_dir):
        return 1
    else:
        return None


def init_repo():
    """Sets up a git repo for this repository."""
    print('\nSetting up Git repository...')
    git_call('init', '.')


def create_gitignore():
    """Creates a .gitignore file.

    The .gitignore file is initialized to ignore itself and the initialization
    files.
    """
    git_ignore(('# Initializing repository',
                '.gitignore',
                'initialize_repository.py',
                'init_repo/'))


if __name__ == '__main__':
    main()  # pragma: no cover
