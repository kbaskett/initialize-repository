.. Initialize Repository documentation master file, created by
   sphinx-quickstart on Sun Apr 24 21:25:03 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Initialize Repository
=====================

Initialize Repository is a set of python scripts that setup a directory
for a new python project. The scripts perform the following functions:

- Initializing a Git repository
- Setting up a new virtual environment for the project
- Setting up a documentation directory with Sphinx for automated 
  documentation builds.
- Setting up a testing repository to contain unit tests that can be run
  Pytest.
- Installing GitHooks to add pre- and post-commit hooks.

The script is extensible, so additional features can easily be added.


Usage
=====

To use this tool, simply download the `repository <https://bitbucket.org/kbaskett/initialize-repository/get/master.zip>`_.
Then, extract ``initialize_repository.py`` and the init_repo folder from 
the archive. Then run initialize_repository.py.

.. warning::
    Extracting the entire archive to the project directory will cause the setup
    scripts to fail. 

.. include:: git_init.rst
.. include:: virtual_env_init.rst
.. include:: docs_init.rst
.. include:: testing_init.rst
.. include:: hooks_init.rst


