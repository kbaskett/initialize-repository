Documentation Initialization
============================

This script sets up the directory for documentation builds using 
`Sphinx`_ as the build system. Documentation is put in the docs 
folder of the project directory. After the folder is set up, 
`Sphinx`_ is installed using pip. If there is a virtual environment
in the project folder, it will be used. 

.. _Sphinx: http://www.sphinx-doc.org /en/stable/index.html

Afterwards, ``sphinx-quickstart`` is used to initialize the 
documentation source and build directories. Finally, the docs 
directory is committed if there is a git repository in the project
folder.
