Testing Initialization
======================

This script sets up testing infrastructure for the project. It performs the 
following actions:

1. Creates a ``tests`` directory to contain all unit tests.
2. Installs `Pytest`_ and `Coverage`_.
3. Configures ``setup.cfg`` to ignore appropriate directories for unit tests.

.. _Pytest: http://pytest.org/latest/
.. _Coverage: http://coverage.readthedocs.io/en/latest/
