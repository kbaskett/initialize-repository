Git Hooks Initialization
========================

This script sets up the pre- and post-commit hooks from `GitHooks`_.
The script downloads the latest version of the project as a zip archive.
The hooks are extracted to a hooks folder in the project directory.
Afterwards, shell scripts are added to ``.git\hooks``. 

.. _GitHooks: https://bitbucket.org/kbaskett/githooks

If the script is run a second time, the `GitHooks`_ package will again be
downloaded. The changeset ID is compared to that of the installed set of 
hooks (as specified in ``setup.cfg``). If the IDs are different, the new set 
of hooks overwrites the old one.


Pre-Commit Hook
---------------

The pre-commit hook performs the following operations:

1. Pep8 conformance is enforced using `Pep8ify`_.
2. Pep8 conformance is then checked using the 
   `Pep8 Python Style Guide Checker`_. If the check fails, the commit 
   is blocked.
3. Finally, unit tests are run using `Pytest`_. If any tests fail, the
   commit is blocked.

The script stashes all unstaged changes to ensure that only those changes
being committed are checked and tested.


.. _Pep8ify: https://github.com/spulec/pep8ify
.. _Pep8 Python Style Guide Checker: https://github.com/PyCQA/pycodestyle
.. _Pytest: http://pytest.org/latest/


Post-Commit Hook
----------------

The post-commit hook performs the following operations:

1. Unit tests are run a second time using `Coverage`_ and `Pytest`_.
   The coverage report is displayed and the html output is recompiled.
2. Documentation is built using `Sphinx`_.

Again, the script stashes uncommitted changes, so only the current state
of the repository is reflected.

.. _Coverage: http://coverage.readthedocs.io/en/latest/
.. _Sphinx: http://www.sphinx-doc.org /en/stable/index.html
