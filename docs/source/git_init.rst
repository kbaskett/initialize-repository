Git Initialization
==================

The Git initialization script sets up a git repository in the current
working directory. It also creates a .gitignore file and adds the 
Initialize Repository setup files to it.
