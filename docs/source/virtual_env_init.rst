Virtual Environment Initialization
==================================

This script sets up a `virtual environment`_ for the project. It is installed
to the project directory in a venv subfolder. If this script runs, then any 
scripts that run later will use the virtual environment when running. It also
adds the venv subfolder to the ``.gitignore`` file.

.. _virtual environment: https://docs.python.org/3/library/venv.html
