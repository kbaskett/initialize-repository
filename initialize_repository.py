"""Runs the initialization scripts in the init_repo folder to initialize the
repository."""

import glob
import os


def main():
    """Main method for the script."""
    print('Initializing repository...')
    modules = get_modules()
    for mod in sorted(modules, key=lambda x: x.SORT_ORDER):
        mod.main()


def get_modules():
    """Return the modules in the init_repo folder that should be run.

    Returns:
        list: A list of modules to run
    """
    mods = [os.path.basename(os.path.splitext(file_)[0]) for file_ in
            glob.iglob('./init_repo/*.py')]
    modules = []
    for mod in mods:
        try:
            module = __import__('init_repo.{0}'.format(mod),
                                fromlist=['.init_repo'])
        except ImportError:
            raise
        else:
            if hasattr(module, 'SORT_ORDER') and hasattr(module, 'main'):
                modules.append(module)
    return modules


if __name__ == '__main__':
    main()
