"""Git pre-commit hook to enforce PEP8 rules and run unit tests.

Copyright (C) Sarah Mount, 2013.
Modifications made by Kenny Baskett, 2016.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
from coverage import Coverage
import os
import re
import subprocess
import sys

import pytest

from utils import stash_files, print_header, top_level_init

MOD_RE = re.compile(r"^[AM]+\s+(?P<name>.*\.py)", re.MULTILINE)


def main():
    """Main method for the script."""
    parser = get_parser()
    args = parser.parse_args()
    all_checks = (not args.enforce_pep8 and not args.check_pep8 and
                  not args.pytest)

    with stash_files():
        staged_files = get_staged_python_files()

        pep8_failed = False
        if staged_files:
            if all_checks or args.enforce_pep8:
                enforce_pep8(staged_files)

            if all_checks or args.check_pep8:
                pep8_failed = check_pep8_conformance(staged_files)

        unit_test_failed = False
        if all_checks or args.pytest:
            unit_test_failed = run_unit_tests()

    # Should we abort this commit?
    if unit_test_failed or pep8_failed:
        print_header('ABORTING COMMIT')
        sys.exit(1)
    else:
        sys.exit(0)


def get_parser():
    """Gets the argument parser for the script.

    Returns:
        argparse.ArgumentParser: The argument parser for the script
    """
    parser = argparse.ArgumentParser(
        description=('Pre-commit script that checks for pep8 conformance and '
                     'runs unit tests using pytest. If none of the optional '
                     'arguments are specified, then all are performed.'))
    parser.add_argument('--enforce-pep8', action='store_true',
                        help='Runs pep8ify to enforce pep8 rules.')
    parser.add_argument('--check-pep8', action='store_true',
                        help=('Checks for pep8 conformance. If the check '
                              'fails, the commit is blocked.'))
    parser.add_argument('--pytest', action='store_true',
                        help='Runs unit tests using pytest.')

    return parser


def get_staged_python_files():
    """Get all files staged for the current commit."""
    output = subprocess.check_output(('git', 'status', '--porcelain'))
    staged_files = MOD_RE.findall(output.decode('utf-8'))
    if staged_files:
        print("Staged Files:")
        for filename in staged_files:
            print("\t" + filename)
    return staged_files


def enforce_pep8(staged_files):
    """Runs pep8ify to enforce pep8 compliance on staged files.

    Arguments:
        staged_files (list): List of files staged in the current commit
    """
    print_header('Enforcing PEP8 Rules')
    for filename in staged_files:
        output = subprocess.check_output(('pep8ify', '-w', filename),
                                         stderr=subprocess.PIPE)
        print(output.decode('utf-8'))
        try:
            os.remove(filename + '.bak')
        except OSError:
            pass
        subprocess.call(('git', 'add', '-u', filename),
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def check_pep8_conformance(staged_files):
    """Checks for pep8 conformance in staged files.

    Arguments:
        staged_files (list): List of files staged in the current commit

    Returns:
        bool: True if the pep8 test failed. False otherwise.
    """
    print_header('Checking PEP8 conformance')
    pep8_failed = False

    for filename in staged_files:
        proc = subprocess.Popen(('pep8', filename), stderr=subprocess.PIPE)
        _, output = proc.communicate()
        # If pep8 still reports problems then abort this commit.
        if output:
            pep8_failed = True
            print_header('Found PEP8 Non-conformance')
            print(output.decode('utf-8'))

    return pep8_failed


def run_unit_tests():
    """Runs unit tests using pytest.

    Returns:
        bool: True if the unit tests failed. False otherwise.
    """
    print_header('Running Unit Tests')
    unit_test_failed = False

    with top_level_init():
        cov = Coverage()
        cov.start()

        result = pytest.main()

        cov.stop()
        cov.save()

        if result != 0:
            print_header('Unit Tests Failed')
            unit_test_failed = True

    return unit_test_failed


if __name__ == '__main__':
    main()
