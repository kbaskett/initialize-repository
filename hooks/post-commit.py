"""Post-commit script for running doc build and coverage."""

import argparse
import contextlib
import os
import subprocess

from utils import (
    stash_files,
    print_header,
    top_level_init,
    virtualenv_active
)


def main():
    """Main method for the script."""
    parser = get_parser()
    args = parser.parse_args()
    perform_all = not (args.doc or args.coverage)

    with stash_files():
        if args.doc or perform_all:
            build_doc()

        if args.coverage or perform_all:
            display_coverage()


def get_parser():
    """Gets the argument parser for the script.

    Returns:
        argparse.ArgumentParser: The argument parser for the script
    """
    parser = argparse.ArgumentParser(
        description=('Post-commit script that builds documentation using '
                     'sphinx and runs a test coverage report. If none of the '
                     'optional arguments are specified, then all are '
                     'performed.'))
    parser.add_argument('-d', '--doc', action='store_true',
                        help=('Build documentation for the repository using '
                              'sphinx.'))
    parser.add_argument('-c', '--coverage', action='store_true',
                        help=('Display coverage report for unit tests.'))
    return parser


@contextlib.contextmanager
def docs_directory():
    """Context manager for switching the current working directory to the docs
    directory and then restoring it on exit.
    """
    cwd = os.getcwd()
    os.chdir(os.path.join(cwd, 'docs'))
    try:
        yield
    finally:
        os.chdir(cwd)


def build_doc():
    """Builds the documentation for the package."""
    print_header('Building Documentation')
    try:
        with virtualenv_active():
            output, errors = _build_doc()
    except NotADirectoryError:
        output, errors = _build_doc()

    if errors:
        print_header('Documentation Build Errors')
        print(errors)
    elif output:
        print_header('Documentation Build Complete')
    else:
        print_header('Documentation Build Failed')


def _build_doc():
    with docs_directory():
        proc = subprocess.Popen(('make.bat', 'html'),
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        output, errors = proc.communicate()
    return (output.decode('utf-8'), errors.decode('utf-8'))


def display_coverage():
    """Runs and displays coverage, and updates html."""
    print_header('Displaying Test Coverage')

    subprocess.call(('coverage', 'html'),
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Display output this way so it shows up in the correct order
    output = subprocess.check_output(('coverage', 'report'))
    print(output.decode('utf-8'))


if __name__ == '__main__':
    main()
